import Test.HUnit
import Codificar

testCodificar = runTestTT testHayQueCodificar
testCuantas = runTestTT testCuantasVecesHayQueCodificar
testLaQueMas = runTestTT testLaQueMasHayQueCodificar
testFrase = runTestTT testCodificarFrase

runAll = testCodificar >> testCuantas >> testLaQueMas >> testFrase


-- runAll = testCantMinuscula >> testMaximoCambios >> testDesplazar >> testCodificar >> testDecodificar


testHayQueCodificar = test [
    "mapeo vacio" ~: hayQueCodificar 'h' [] ~?= False,
    "c es igual a la 2da componente" ~: hayQueCodificar 'h' [('j', 'h'), ('o', 'g')] ~?= False,
    "c no esta en mapeo" ~: hayQueCodificar 'h' [('o', 'p')] ~?= False,
    "c igual a la 1ra componente" ~: hayQueCodificar 'h' [('o', 'l'), ('h', 'g')] ~?= True
    ]   

testCuantasVecesHayQueCodificar = test [
    "hayQueCodificar = false" ~: cuantasVecesHayQueCodificar 'h' "hola" [('o', 'h')] ~?= 0,
    "hayQueCodificar = true, c aparece 1 vez" ~: cuantasVecesHayQueCodificar 'h' "hola" [('h', 'o')] ~?= 1,
    "hayQueCodificar = true, c aparece + de 1 vez" ~: cuantasVecesHayQueCodificar 'o' "hola como" [('o', 'r'), ('a', 'i')] ~?= 3
    ]

testLaQueMasHayQueCodificar = test
    [ "Un solo caracter que se puede codificar" ~: laQueMasHayQueCodificar "Hola" [('r', 'a'), ('o', 'm')] ~?= 'o',
      "Varios caracteres se puede codificar, uno solo aparece mas veces" ~: laQueMasHayQueCodificar "Testeando" [('s', 'z'), ('e', 'l'), ('a', 'k')] ~?= 'e',
      "Varios caracteres se pueden codificar, varios aparecen la misma cantidad de veces" ~: laQueMasHayQueCodificar "Hola gato" [('l', 'm'), ('o', 'a'), ('a', 'z')] ~?= 'o'
    ]

testCodificarFrase =
  test
    [ "Mapeo vacío" ~: codificarFrase "hola" [] ~?= "hola",
      "Frase no tiene caracteres codificables" ~: codificarFrase "hola" [('t', 'p'), ('x', 'e')] ~?= "hola",
      "Frase tiene algunos caracteres codificables" ~: codificarFrase "TesteaNdo" [('e', ' '), ('N', 'P'), ('r', 'T')] ~?= "T st aPdo",
      "Frase se puede codificar toda" ~: codificarFrase "PruebA" [('r', 'a'), ('P', 'G'), ('u', 't'), ('b', 's'), ('e', 'o'), ('A', '!')] ~?= "Gatos!"
    ]
