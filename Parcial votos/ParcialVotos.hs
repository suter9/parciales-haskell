{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use foldr" #-}
{-# HLINT ignore "Eta reduce" #-}

module ParcialVotos where
-- * --- --- --- --- EJERCICIO 1: Votos en Blanco --- --- --- --- * --
votosEnBlanco :: [(String, String)] -> [Int] -> Int -> Int
votosEnBlanco _ [] cantTotalVotos = cantTotalVotos
votosEnBlanco _ votos cantTotalVotos = cantTotalVotos - suma votos

suma :: [Int] -> Int
suma [] = 0
suma (x:xs) = x + suma xs

-- * --- --- --- --- EJERCICIO 2: Formulas Validas --- --- --- --- * --
formulasValidas :: [(String, String)] -> Bool
formulasValidas [] = True
formulasValidas formulas = not (seRepiteCandidato (aplanar formulas))

aplanar :: [(String, String)] -> [String]
aplanar [] = []
aplanar ((a, b):xs) = a : b : aplanar xs

seRepiteCandidato ::  [String] -> Bool -- si candidato no pertenece, sigue haciendo recursion hasta vaciar lista ([])
seRepiteCandidato [] = False
seRepiteCandidato (candidato:xs) = candidato `pertenece` xs || seRepiteCandidato xs

pertenece :: (Eq t) => t -> [t] -> Bool
pertenece _ [] = False
pertenece elem (x:xs) = elem == x || pertenece elem xs

-- * --- --- --- --- EJERCICIO 3: Porcentaje de Votos --- --- --- --- * --
{-Resumen: tengo que buscar la posicion de la tupla del presidente y con eso busco sus votos en 'votos'
    porcentaje de votos -> es sumar todos los votos  y hacer (obtenerVotosDe/totalVotos) * 100 -} 
porcentajeDeVotos :: String -> [(String, String)] -> [Int] -> Float
porcentajeDeVotos presidente formulas votos = division (obtenerVotosDe presidente formulas votos) (suma votos) * 100 

-- TODO: Devuelve la cantidad de votos del presidente indicado.
obtenerVotosDe :: String -> [(String, String)] -> [Int] -> Int
obtenerVotosDe presidente formulas votos = obtenerElementoEn (posicionPresidente presidente formulas) votos

-- TODO: Voy sumando 1 hasta encontrar la posicion de la tupla que contiene al presidente indicado.
posicionPresidente :: String -> [(String, String)] -> Int -- posicion >= 1, sumo hasta encontrar al presidente
posicionPresidente nombre ((a, _):xs) | nombre == a = 1 
                                      | otherwise = 1 + posicionPresidente nombre xs

-- TODO: Me devuelve el elemento en dicha posicion
obtenerElementoEn :: (Eq t) => Int -> [t] -> t -- voy quitando el head de la lista y resto 1 a posicion, hasta llegar a 1
obtenerElementoEn 1 (x:_) = x -- si posicion es = 1, devuelvo head
obtenerElementoEn posicion (_:xs) = obtenerElementoEn (posicion - 1) xs

division :: Int -> Int -> Float
division a b = fromIntegral a / fromIntegral b

-- * --- --- --- --- EJERCICIO 4: Proximo Presidente --- --- --- --- * --
-- busco la posicion de la cantidad de votos mas alta en "votos" 
-- con esa posicion busco la tupla y obtengo el presidente
proximoPresidente :: [(String, String)] -> [Int] -> String
proximoPresidente formulas votos = fst (obtenerElementoEn (obtenerPosicionDe (maximo votos) votos) formulas)

-- TODO: Devuelve la posicion dentro de una lista del elemento indicado
obtenerPosicionDe :: (Eq t) => t -> [t] -> Int
obtenerPosicionDe elem (x:xs) | elem == x = 1 
                              | otherwise = 1 + obtenerPosicionDe elem xs

maximo :: [Int] -> Int
maximo [x] = x
maximo (x:y:xs) | x >= y = maximo (x:xs)
                | otherwise = maximo (y:xs)



