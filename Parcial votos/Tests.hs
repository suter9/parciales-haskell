import Test.HUnit
import ParcialVotos

testVBlancos = runTestTT testVotosEnBlanco
testFormulas = runTestTT testFormulasValidas
testPorcentaje = runTestTT testPorcentajeDeVotos
testProximo = runTestTT testProximoPresidente

testVotosEnBlanco = test [
    "votos[] y formulas[] vacio" ~: votosEnBlanco [] [] 700 ~?= 700,
    "Todos son votos en blanco" ~: votosEnBlanco [("Juan", "Maria"), ("Dario", "Ivan")] [0, 0] 100 ~?= 100,
    "1 formula totalVotos=votos" ~: votosEnBlanco [("Juan", "Maria")] [350] 350 ~?= 0,
    "3 formulas" ~: votosEnBlanco [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] [300,0,250] 600 ~?= 50
    ]   

testFormulasValidas = test [
    "Formula vacia" ~: formulasValidas [] ~?= True,
    "Mismo candidato en presidente y vicepresidente" ~: formulasValidas [("Juan", "Juan"), ("Dario", "Ivan")] ~?= False,
    "Candidato repetido en distintas formulas" ~: formulasValidas [("Juan", "Sol"), ("Dario", "Juan")] ~?= False,
    "Formula valida" ~: formulasValidas [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] ~?= True
    ]

testPorcentajeDeVotos = test [
    "Una sola formula" ~: porcentajeDeVotos "Juan" [("Juan", "Maria")] [450] ~?= 100,
    "Cuatro formulas misma cantidad de votos" ~: porcentajeDeVotos "Sol" [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura"), ("Daniel", "Jose")] [500,500,500, 500] ~?= 25,
    "Distinta cantidades de votos" ~: porcentajeDeVotos "Dario" [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] [200,500,300] ~?= 50
    ]

testProximoPresidente = test [
    "Una formula" ~: proximoPresidente [("Juan", "Dario")] [50] ~?= "Juan",
    "3 formulas" ~: proximoPresidente [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] [50,70,38] ~?= "Dario",
    "Empate" ~: proximoPresidente [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] [30,30,30] ~?= "Juan"
    ]