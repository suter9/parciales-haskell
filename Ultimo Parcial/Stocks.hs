
-- * EJERCICIO 1
-- generarStock :: [[Char]] -> [([Char], Int)]
-- generarStock listaProductos = generarStockAux (eliminarRepetidos listaProductos) listaProductos

-- generarStockAux :: [[Char]] -> [[Char]] -> [([Char], Int)]
-- generarStockAux [] _ = [] 
-- generarStockAux (producto:ps) listaOriginal =  (producto, cantidadApariciones producto listaOriginal) : generarStockAux ps listaOriginal



eliminarRepetidos :: [[Char]] -> [[Char]]
eliminarRepetidos [] = []
eliminarRepetidos (producto:ps) | producto `pertenece` ps = eliminarRepetidos (producto : quitarTodos producto ps)
                                | otherwise = producto : eliminarRepetidos ps

quitar :: [Char] -> [[Char]] -> [[Char]]
quitar _ [] = []
quitar producto (p:ps) | producto == p = ps
                       | otherwise = p : quitar producto ps

quitarTodos :: [Char] -> [[Char]] -> [[Char]]
quitarTodos producto listaProductos | producto `pertenece` listaProductos = quitarTodos producto (quitar producto listaProductos)
                                    | otherwise = listaProductos

pertenece :: [Char] -> [[Char]] -> Bool
pertenece _ [] = False
pertenece producto (p:ps) = producto == p  || pertenece producto ps


{-
eliminarRepetidos y cantidadApariciones sobre la original

|res| = |productos sin repetir|
primera componente de res[i] = producto, la segunda componente = cuantas veces se repite el producto en productos
-}

-- * EJERCICIO 2
stockDeProducto :: [([Char], Int)] -> [Char] -> Int
stockDeProducto [] _ = 0
stockDeProducto ((producto, cantidad):xs) p | p == producto = cantidad
                                            | otherwise = stockDeProducto xs p
{-
res = 0 si producto no esta en la 1er componente de stock. Sino devuelve la cantidad del producto
-}

-- * EJERCICIO 3
dineroEnStock :: [([Char], Int)] -> [([Char], Float)] -> Float
dineroEnStock [] _ = 0
dineroEnStock ((producto, cantidad):xs) precios = obtenerPrecio producto precios * fromIntegral cantidad + dineroEnStock xs precios

obtenerPrecio :: [Char] -> [([Char], Float)] -> Float
obtenerPrecio producto ((p, precio):xs) | producto == p = precio
                                        | otherwise = obtenerPrecio producto xs

{-
precio1 * cantidad1 + precio2 * cantidad2
-}

-- * EJERCICIO 4
aplicarOferta :: [([Char], Int)] -> [([Char], Float)] -> [([Char], Float)]
aplicarOferta _ [] = []
aplicarOferta stock ((producto, precio):ys) | stockDeProducto stock producto > 10 = (producto, precio * 0.8) : aplicarOferta stock ys
                                            | otherwise = (producto, precio) : aplicarOferta stock ys

-- * PUNTO 1 devuelta
generarStock :: [[Char]] -> [([Char], Int)]
generarStock listaProductos = generarStockAux listaProductos listaProductos []

generarStockAux :: [[Char]] -> [[Char]] -> [([Char], Int)] -> [([Char], Int)]
generarStockAux [] _ stock = stock
generarStockAux (producto:ps) listaOriginal stock | producto `existeEn` stock = generarStockAux ps listaOriginal stock
                                                  | otherwise = generarStockAux ps listaOriginal (stock ++ [(producto, cantidadApariciones producto listaOriginal)])

existeEn :: [Char] -> [([Char], Int)] -> Bool
existeEn _ [] = False
existeEn producto ((p, _):xs) = producto == p || existeEn producto xs

cantidadApariciones :: [Char] -> [[Char]] -> Int
cantidadApariciones _ [] = 0
cantidadApariciones producto (p:ps) | producto == p = 1 + cantidadApariciones producto ps
                                    | otherwise = cantidadApariciones producto ps



-- voy armando las tuplas
-- pregunto si x pertenece a las tuplas ya creadas
-- si pertenece devuelvo [] y sigo hasta vaciar todo



{-
Si stockDeProducto stock (1ra componente precio[osea un producto]) > 10 entonces tengo que agregar tuplas en las que
la primera componente tienen que ser igual a la 1ra componente de precios [osea un producto] y
la segunda componente = precio * 0.8

Caso contrario (stock <= 10), la primera componente de la tupla tiene que ser el producto y la 2da el precio normal.
-}




-- divisoresPropios :: Int -> [Int]
-- divisoresPropios n = divisoresPropiosAux n 1

-- divisoresPropiosAux :: Int -> Int -> [Int]
-- divisoresPropiosAux n contador | n == contador = []
--                                | esDivisor n contador = contador : divisoresPropiosAux n (contador+1)
--                                | otherwise = divisoresPropiosAux n (contador+1)

-- esDivisor :: Int -> Int -> Bool
-- esDivisor n divisor = n `mod` divisor == 0

-- -- Ejercicio 2
-- sonAmigos :: Int -> Int -> Bool
-- sonAmigos n m = suma (divisoresPropios n) == m && suma (divisoresPropios m) == n

-- suma :: [Int] -> Int
-- suma [] = 0
-- suma (x:xs) = x + suma xs

-- -- EJERCICIO 3
-- losPrimerosNPerfectos :: Int -> [Int]
-- losPrimerosNPerfectos n = losPrimerosNPerfectosAux n 1 []

-- losPrimerosNPerfectosAux :: Int -> Int -> [Int] -> [Int]
-- losPrimerosNPerfectosAux n contador listaPerfectos
--     | longitud listaPerfectos == n = listaPerfectos
--     | esNumeroPerfecto contador = losPrimerosNPerfectosAux n (contador+1) (listaPerfectos ++ [contador])
--     | otherwise = losPrimerosNPerfectosAux n (contador+1) listaPerfectos

-- longitud :: [Int] -> Int
-- longitud [] = 0
-- longitud (x:xs) = 1 + longitud xs

-- esNumeroPerfecto :: Int -> Bool
-- esNumeroPerfecto n = suma (divisoresPropios n) == n

-- -- EJERCICIO 4
-- listaDeAmigos :: [Int] -> [(Int, Int)]
-- listaDeAmigos [] = []
-- listaDeAmigos (x:xs) = obtenerAmigos x xs ++ listaDeAmigos xs

-- obtenerAmigos :: Int -> [Int] -> [(Int, Int)]
-- obtenerAmigos _ [] = []
-- obtenerAmigos x (y:ys) | sonAmigos x y = (x, y) : obtenerAmigos x ys
--                        | otherwise = obtenerAmigos x ys