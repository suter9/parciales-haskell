{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Evaluate" #-}
{-# HLINT ignore "Use foldr" #-}

-- Ejercicio 1

relacionesValidas :: [(String, String)] -> Bool
relacionesValidas [] = True
relacionesValidas (x:xs) 
    | tieneTuplasRepetidas (x:xs) || tieneComponentesIguales x = False
    | otherwise = relacionesValidas xs

tieneTuplasRepetidas :: [(String, String)] -> Bool
tieneTuplasRepetidas [] = False
tieneTuplasRepetidas (x:xs) = tieneTuplasRepetidasAux x xs || tieneTuplasRepetidas xs

tieneTuplasRepetidasAux :: (String, String) -> [(String, String)] -> Bool
tieneTuplasRepetidasAux _ [] = False
tieneTuplasRepetidasAux relacion (x:xs) = sonTuplasIguales relacion x || tieneTuplasRepetidasAux relacion xs

sonTuplasIguales :: (String, String) -> (String, String) -> Bool
sonTuplasIguales (a, b) (c, d) = (a == c && b == d) || (a == d && b == c)

tieneComponentesIguales :: (String, String) -> Bool
tieneComponentesIguales (persona1, persona2) = persona1 == persona2

-- Ejercicio 2

personas :: [(String, String)] -> [String]
personas relaciones = eliminarRepetidos (obtenerListaPersonas relaciones)

obtenerListaPersonas :: [(String, String)] -> [String]
obtenerListaPersonas [] = []
obtenerListaPersonas ((a, b):xs) = a:b:[] ++ obtenerListaPersonas xs

eliminarRepetidos :: [String] -> [String]
eliminarRepetidos [] = []
eliminarRepetidos (x:xs)
    | x `pertenece` xs = x : eliminarRepetidos (quitarTodos x xs)
    | otherwise = x : eliminarRepetidos xs

pertenece :: (Eq t) => t -> [t] -> Bool
pertenece _ [] = False
pertenece x (y:ys) = x == y || pertenece x ys

quitarTodos :: (Eq t) => t -> [t] -> [t]
quitarTodos _ [] = []
quitarTodos x lista
    | x `pertenece` lista = quitarTodos x (quitar x lista) 
    | otherwise =  lista

quitar :: (Eq t) => t -> [t] -> [t]
quitar _ [] = []
quitar x (y:ys)
    | not (x `pertenece` (y:ys)) = y:ys
    | x == y = ys
    | otherwise = y : quitar x ys

-- Ejercicio 3

amigosDe :: String -> [(String, String)] -> [String]
amigosDe _ [] = []
amigosDe persona ((a, b):xs)
    | persona == a = b : amigosDe persona xs
    | persona == b = a : amigosDe persona xs
    | otherwise = amigosDe persona xs

-- Ejercicio 4

personaConMasAmigos :: [(String, String)] -> String
personaConMasAmigos relaciones = personaConMasAmigosAux (personas relaciones) "nadie" relaciones

personaConMasAmigosAux :: [String] -> String ->  [(String, String)] -> String
personaConMasAmigosAux [x] masAmistoso _ = masAmistoso
personaConMasAmigosAux (p1:personas) masAmistoso relaciones
    | longitud (amigosDe p1 relaciones) >= longitud (amigosDe masAmistoso relaciones) = personaConMasAmigosAux personas p1 relaciones
    | otherwise = personaConMasAmigosAux personas masAmistoso relaciones

longitud :: (Eq t) => [t] -> Integer
longitud [] = 0
longitud (x:xs) = 1 + longitud xs