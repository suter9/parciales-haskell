import Simulacro
import Test.HUnit


testRelVal = runTestTT testRelacionesValidas
testPers = runTestTT testPersonas
testAmigos = runTestTT testAmigosDe
testMasAmigos = runTestTT testPersonaConMasAmigos

testRelacionesValidas = test [
    "Relaciones[] vacio" ~: relacionesValidas [] ~?= True,
    "Tuplas repetidas" ~: relacionesValidas [("ana", "pedro"), ("matias", "juan"), ("ana", "pedro")] ~?= False,
    "Tuplas repetidas invertidas" ~: relacionesValidas [("ana", "pedro"), ("matias", "juan"), ("pedro", "ana")] ~?= False,
    "Componentes iguales" ~: relacionesValidas[("ana", "pedro"), ("matias", "matias"), ("pedro", "juan")] ~?= False,
    "Ni repetido ni comp. iguales" ~: relacionesValidas[("ana", "pedro"), ("matias", "ivan"), ("ana", "juan")] ~?= True
    ]

testPersonas = test [
    "Relaciones[] vacio" ~: personas [] ~?= [],
    "Con nombres repetidos" ~: personas [("ana", "pedro"), ("matias", "pedro"), ("ana", "juan")] ~?= ["ana", "pedro", "matias", "juan"],
    "Sin repetir nombres" ~: personas [("ana", "pedro"), ("matias", "juan")] ~?= ["ana", "pedro", "matias", "juan"]
    ]

testAmigosDe = test [
    "Relaciones[] vacio" ~: amigosDe "ana" [] ~?= [],
    "Con amigos" ~: amigosDe "ana" [("ana", "juan"), ("pedro", "ana")] ~?= ["juan", "pedro"],
    "Sin amigos" ~: amigosDe "ivan" [("ana", "juan"), ("pedro", "ana")] ~?= []
    ]

testPersonaConMasAmigos = test [
    "Empate" ~: personaConMasAmigos [("ana", "pedro")] ~?= "ana",
    "La primera persona tiene mas amigos" ~: personaConMasAmigos [("ana", "juan"), ("pedro", "ivan"), ("matias", "ana")] ~?= "ana",
    "Otro que no sea el 1ero tiene mas amigos" ~: personaConMasAmigos [("ana", "juan"), ("pedro", "ivan"), ("pedro", "sol")] ~?= "pedro"
    ]