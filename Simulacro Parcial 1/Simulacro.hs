{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Evaluate" #-}
{-# HLINT ignore "Use foldr" #-}

module Simulacro where

{-
Devuelve true si no tiene tuplas repetidas ni tuplas con componentes iguales

Tuplas con componentes iguales:
    - comparo si a == b

Tuplas repetidas
    - agarro una tupla y voy comparando con las demas
-}

relacionesValidas :: [(String, String)] -> Bool
relacionesValidas [] = True
relacionesValidas (x:xs) 
    | tieneTuplasRepetidas (x:xs) || tieneComponentesIguales x = False
    | otherwise = relacionesValidas xs


{-
    * Manda una relacion (x) a "Aux" para que vaya iterando y evaluando si es igual a otras tuplas (xs)
    * En el caso de que Aux sea falsa, hago recursion con tieneTuplasRepetidas (mando xs como parametro)
    * y seguir comparando las demas relaciones.
-}
tieneTuplasRepetidas :: [(String, String)] -> Bool
tieneTuplasRepetidas [] = False
tieneTuplasRepetidas (x:xs) = tieneTuplasRepetidasAux x xs || tieneTuplasRepetidas xs

{-
    * Como 1er parametro recibe una relacion fija (tupla) 
    * y va comparando con el resto de las relaciones (2do parametro)
-} 
tieneTuplasRepetidasAux :: (String, String) -> [(String, String)] -> Bool
tieneTuplasRepetidasAux _ [] = False
tieneTuplasRepetidasAux relacion (x:xs) = tuplasIguales relacion x || tieneTuplasRepetidasAux relacion xs


tuplasIguales :: (String, String) -> (String, String) -> Bool
tuplasIguales (a, b) (c, d) = (a == c && b == d) || (a == d && b == c)


tieneComponentesIguales :: (String, String) -> Bool
tieneComponentesIguales (persona1, persona2) = persona1 == persona2



----------------------------------------

{-
Tengo que buscar una forma de pasar una lista de tuplas a una lista de String. Y dsp veo como
eliminar los duplicados.

Pasar de [(String, String)] -> [String] hecho

para eliminar repetidos:
    - agarro uno y pregunto si pertenece, si pertenece lo borro

-}

personas :: [(String, String)] -> [String]
personas relaciones = eliminarRepetidos (obtenerListaPersonas relaciones)

obtenerListaPersonas :: [(String, String)] -> [String]
obtenerListaPersonas [] = []
obtenerListaPersonas ((a, b):xs) = a:b:[] ++ obtenerListaPersonas xs

eliminarRepetidos :: [String] -> [String]
eliminarRepetidos [] = []
eliminarRepetidos (x:xs)
    | x `pertenece` xs = x : eliminarRepetidos (quitarTodos x xs)
    | otherwise = x : eliminarRepetidos xs


pertenece :: (Eq t) => t -> [t] -> Bool
pertenece _ [] = False
pertenece x (y:ys) = x == y || pertenece x ys


quitarTodos :: (Eq t) => t -> [t] -> [t]
quitarTodos _ [] = []
quitarTodos x lista
    | x `pertenece` lista = quitarTodos x (quitar x lista) 
    | otherwise =  lista


quitar :: (Eq t) => t -> [t] -> [t]
quitar _ [] = []
quitar x (y:ys)
    | not (x `pertenece` (y:ys)) = y:ys
    | x == y = ys
    | otherwise = y : quitar x ys



-----------------------------------------------------------------------------------

amigosDe :: String -> [(String, String)] -> [String]
amigosDe _ [] = []
amigosDe persona ((a, b):xs)
    | persona == a = b : amigosDe persona xs
    | persona == b = a : amigosDe persona xs
    | otherwise = amigosDe persona xs

-----------------------------------------------------------------------------------

{-
yo con las personas le puedo pedir cuantos amigos tienen
despues de eso le pido la longitud
-}

personaConMasAmigos :: [(String, String)] -> String
personaConMasAmigos relaciones = personaConMasAmigosAux (personas relaciones) "nadie" relaciones

personaConMasAmigosAux :: [String] -> String ->  [(String, String)] -> String
personaConMasAmigosAux [] masAmistoso _ = masAmistoso
personaConMasAmigosAux (p1:personas) masAmistoso relaciones
    | longitud (amigosDe p1 relaciones) >= longitud (amigosDe masAmistoso relaciones) = personaConMasAmigosAux personas p1 relaciones
    | otherwise = personaConMasAmigosAux personas masAmistoso relaciones


longitud :: (Eq t) => [t] -> Integer
longitud [] = 0
longitud (x:xs) = 1 + longitud xs