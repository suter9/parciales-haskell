{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use foldr" #-}
{-# HLINT ignore "Eta reduce" #-}
module ParcialVotos2 where
-- * --- --- --- --- EJERCICIO 1: Votos Afirmativos --- --- --- --- * --
{-Resumen:  Porcentaje no blancos: (votosNoBlanco/totalVotos) * 100 -} 
porcentajeDeVotosAfirmativos :: [(String, String)] -> [Int] -> Int -> Float
porcentajeDeVotosAfirmativos _ _ 0 = 0
porcentajeDeVotosAfirmativos _ votos cantTotalDeVotos = division (suma votos) cantTotalDeVotos * 100

suma :: [Int] -> Int
suma [] = 0
suma (x:xs) = x + suma xs

division :: Int -> Int -> Float
division a b = fromIntegral a / fromIntegral b

-- * --- --- --- --- EJERCICIO 2: Formulas Invalidas --- --- --- --- * --
formulasInvalidas :: [(String, String)] -> Bool
formulasInvalidas [] = False
formulasInvalidas ((presidente, vicepresidente):xs) = presidente == vicepresidente || seRepitenCandidatos (presidente, vicepresidente) xs || formulasInvalidas xs

seRepitenCandidatos :: (String, String) -> [(String, String)] -> Bool
seRepitenCandidatos _ [] = False
seRepitenCandidatos (presi, vice) ((a, b):xs)
    | presi == a || presi == b || vice == a || vice == b = True
    | otherwise = seRepitenCandidatos (presi, vice) xs

-- * --- --- --- --- EJERCICIO 3: Porcentaje de Votos --- --- --- --- * --
porcentajeDeVotos :: String -> [(String, String)] -> [Int] -> Float
porcentajeDeVotos vice formulas votos = division (obtenerVotosDe vice formulas votos) (suma votos) * 100 

-- TODO: Devuelve la cantidad de votos del vice indicado.
obtenerVotosDe :: String -> [(String, String)] -> [Int] -> Int
obtenerVotosDe vice formulas votos = obtenerElementoEn (posicionVice vice formulas) votos

-- TODO: Voy sumando 1 hasta encontrar la posicion de la tupla que contiene al vice indicado.
posicionVice :: String -> [(String, String)] -> Int -- posicion >= 1, sumo hasta encontrar al vice
posicionVice nombre ((_, b):xs) | nombre == b = 1 
                                | otherwise = 1 + posicionVice nombre xs

-- TODO: Me devuelve el elemento en dicha posicion
obtenerElementoEn :: (Eq t) => Int -> [t] -> t -- voy quitando el head de la lista y resto 1 a posicion, hasta llegar a 1
obtenerElementoEn 1 (x:_) = x -- si posicion es = 1, devuelvo head
obtenerElementoEn posicion (_:xs) = obtenerElementoEn (posicion - 1) xs

-- * --- --- --- --- EJERCICIO 4: Menos Votado --- --- --- --- * --
menosVotado :: [(String, String)] -> [Int] -> String
menosVotado formulas votos = fst (obtenerElementoEn (obtenerPosicionDe (minimo votos) votos) formulas)

-- TODO: Devuelve la posicion dentro de una lista del elemento indicado
obtenerPosicionDe :: (Eq t) => t -> [t] -> Int
obtenerPosicionDe elem (x:xs) | elem == x = 1 
                              | otherwise = 1 + obtenerPosicionDe elem xs

minimo :: [Int] -> Int
minimo [x] = x
minimo (x:y:xs) | x <= y = minimo (x:xs)
                | otherwise = minimo (y:xs)

