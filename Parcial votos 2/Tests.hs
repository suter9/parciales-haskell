import Test.HUnit
import ParcialVotos2

testAfirmativos = runTestTT testPorcentajeDeVotosAfirmativos
testFormulas = runTestTT testFormulasInvalidas
testPorcentaje = runTestTT testPorcentajeDeVotos
testMenos = runTestTT testMenosVotado

expectAny actual expected = elem actual expected ~? ("expected any of: " ++ show expected ++ "In but got: " ++ show actual)


testPorcentajeDeVotosAfirmativos = test [
    "votos[] y formulas[] vacio" ~: porcentajeDeVotosAfirmativos [] [] 0 ~?= 0,
    "Todos son votos en blanco" ~: porcentajeDeVotosAfirmativos [("Juan", "Maria"), ("Dario", "Ivan")] [0, 0] 100 ~?= 0,
    "Sin votos en blanco" ~: porcentajeDeVotosAfirmativos [("Juan", "Maria")] [350] 350 ~?= 100,
    "3 formulas" ~: porcentajeDeVotosAfirmativos [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] [500,0,250] 1000 ~?= 75
    ]   

testFormulasInvalidas = test [
    "Formula vacia" ~: formulasInvalidas [] ~?= False,
    "Mismo candidato en presidente y vicepresidente" ~: formulasInvalidas [("Juan", "Juan"), ("Dario", "Ivan")] ~?= True,
    "Candidato repetido en distintas formulas" ~: formulasInvalidas [("Juan", "Sol"), ("Dario", "Juan")] ~?= True,
    "Todos distintos" ~: formulasInvalidas [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] ~?= False
    ]

testPorcentajeDeVotos = test [
    "Una sola formula" ~: porcentajeDeVotos "Maria" [("Juan", "Maria")] [450] ~?= 100,
    "Cuatro formulas misma cantidad de votos" ~: porcentajeDeVotos "Laura" [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura"), ("Daniel", "Jose")] [500,500,500, 500] ~?= 25,
    "Distinta cantidades de votos" ~: porcentajeDeVotos "Ivan" [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] [200,500,300] ~?= 50
    ]

testMenosVotado = test [
    "Una formula" ~: menosVotado [("Juan", "Dario")] [50] ~?= "Juan",
    "3 formulas" ~: menosVotado [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] [40,50,38] ~?= "Sol",
    "Empate" ~: expectAny (menosVotado [("Juan", "Maria"), ("Dario", "Ivan"), ("Sol", "Laura")] [30,30,30]) ["Juan", "Dario", "Sol"]
    ]