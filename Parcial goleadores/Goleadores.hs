{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use foldr" #-}

module Goleadores where
-- * --- --- --- --- EJERCICIO 1: Atajaron Suplentes --- --- --- --- * --
atajaronSuplentes :: [(String, String)] -> [Int] -> Int -> Int
atajaronSuplentes _ goles totalGolesTorneo = totalGolesTorneo - golesTitulares goles

golesTitulares :: [Int] -> Int
golesTitulares [] = 0
golesTitulares (x:xs) = x + golesTitulares xs

-- * --- --- --- --- EJERCICIO 2: Equipos Válidos --- --- --- --- * --
equiposValidos :: [(String, String)] -> Bool
equiposValidos arquerosPorEquipo = not (hayRepetidos (aplanar arquerosPorEquipo))

hayRepetidos :: [String] -> Bool
hayRepetidos [] = False
hayRepetidos (x:xs) = x `pertenece` xs || hayRepetidos xs

aplanar :: [(String, String)] -> [String]
aplanar [] = []
aplanar ((a, b):xs) = a : b : aplanar xs

pertenece :: String -> [String] -> Bool
pertenece _ [] = False
pertenece elem (x:xs) = elem == x || pertenece elem xs

-- * --- --- --- --- EJERCICIO 3: Porcentaje de Goles --- --- --- --- * --
-- porcentaje = los que se comio/ total goles * 100
porcentajeDeGoles :: String -> [(String, String)] -> [Int] -> Float
porcentajeDeGoles arquero arquerosPorEquipo goles = division (golesDe arquero arquerosPorEquipo goles) (golesTitulares goles) * 100

golesDe :: String -> [(String, String)] -> [Int] -> Int
golesDe _ _ [x] = x
golesDe arquero ((_, b):xs) (y:ys) | arquero == b = y
                                   | otherwise = golesDe arquero xs ys

division :: Int -> Int -> Float
division a b = fromIntegral a / fromIntegral b
 


-- * --- --- --- --- EJERCICIO 4: Valla Menos Vencida --- --- --- --- * --
vallaMenosVencida :: [(String, String)] -> [Int] -> String
vallaMenosVencida [(_, arquero)] [g] = arquero
vallaMenosVencida (x:y:xs) (g:z:gs) | g <= z = vallaMenosVencida (x:xs) (g:gs)
                                    | otherwise = vallaMenosVencida (y:xs) (z:gs)


{-
 aPE = [ a, x, ,b]
 goles = [ a, x , b]
-}
-- ? ------------------- PARCIAL QUE HIZO EL PROFE EN CLASE
-- * --- --- --- --- EJERCICIO 1: Goles de No Goleadores --- --- --- --- * --
golesDeNoGoleadores :: [(String, String)] -> [Int] -> Int -> Int
golesDeNoGoleadores _ goles golesTorneo = golesTorneo - golesGoleadores  goles

golesGoleadores :: [Int] -> Int
golesGoleadores [] = 0
golesGoleadores (x:xs) = x + golesGoleadores xs


-- * --- --- --- --- EJERCICIO 2: Equipos Validos --- --- --- --- * --
-- es lo mismo que el parcial anterior

-- * --- --- --- --- EJERCICIO 4: Botin de Oro --- --- --- --- * --
botinDeOro :: [(String, String)] -> [Int] -> String
botinDeOro [(_, goleador)] [_] = goleador
botinDeOro (x:y:xs) (g:z:gs) | g >= z = botinDeOro (x:xs) (g:gs)
                             | otherwise = botinDeOro (y:xs) (z:gs)

