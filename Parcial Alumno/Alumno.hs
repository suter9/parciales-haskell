{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Use foldr" #-}
{-# HLINT ignore "Use >=" #-}
module Alumno where
-- * EJERCICIO 1: Materias Aprobadas 
aproboMasDeNMaterias :: [([Char], [Int])] -> [Char] -> Int -> Bool
aproboMasDeNMaterias ((nombre, notas):registros) alumno n | nombre == alumno = cantidadAprobadas notas > n
                                                          | otherwise = aproboMasDeNMaterias registros alumno n

cantidadAprobadas :: [Int] -> Int
cantidadAprobadas [] = 0
cantidadAprobadas (nota:notas) | nota >= 4 = 1 + cantidadAprobadas notas
                               | otherwise = cantidadAprobadas notas

-- -- * EJERCICIO 2: Buenos Alumnos
buenosAlumnos :: [([Char], [Int])] -> [[Char]]
buenosAlumnos [] = []
buenosAlumnos ((alumno, notas):registros) | promedio notas >= 8 && tieneTodoAprobado notas = alumno : buenosAlumnos registros
                                          | otherwise = buenosAlumnos registros

promedio :: [Int] -> Float
promedio notas = suma notas `division` longitud notas

tieneTodoAprobado :: [Int] -> Bool
tieneTodoAprobado [] = True
tieneTodoAprobado (nota:notas) = not (nota < 4) && tieneTodoAprobado notas

suma :: [Int]  -> Int
suma [] = 0
suma (x:xs) = x + suma xs

longitud :: [Int] -> Int
longitud [] = 0
longitud (x:xs) = 1 + longitud xs

division :: Int -> Int -> Float
division a b = fromIntegral a / fromIntegral b

-- * EJERCICIO 3: Mejor Promedio
mejorPromedio :: [([Char], [Int])] -> [Char]
mejorPromedio [(alumno, _)] = alumno
mejorPromedio ((alumno1, notas1):(alumno2, notas2):registros) | promedio notas1 >= promedio notas2 = mejorPromedio ((alumno1, notas1):registros)
                                                              | otherwise = mejorPromedio ((alumno2, notas2):registros)

-- * EJERCICIO 4: Graduado con Honores
seGraduoConHonores :: [([Char], [Int])] -> Int -> String -> Bool
seGraduoConHonores registro cantidadMaterias alumno = aproboTodo && esBuenAlumno && cumplePromedio
    where
        aproboTodo = aproboMasDeNMaterias registro alumno (cantidadMaterias - 1)
        esBuenAlumno = alumno `pertenece` buenosAlumnos registro
        cumplePromedio = promedio (obtenerNotas alumno registro) > promedio (obtenerNotas (mejorPromedio registro) registro) - 1

obtenerNotas :: String -> [([Char], [Int])] -> [Int]
obtenerNotas alumno ((nombre, notas):xs) | alumno == nombre = notas
                                         | otherwise = obtenerNotas alumno xs

pertenece :: String -> [String] -> Bool
pertenece _ [] = False
pertenece nombre (alumno:alumnos) = nombre == alumno || pertenece nombre alumnos


-- aprobomasDeNMaterias cantiMat-1 = true y alumno pertenece a buenosAlumnos y promedio de notas esta a menos de un punto del mejorPromedio